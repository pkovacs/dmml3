import matplotlib.pyplot as plt
import numpy as np


def sort_matrix_by_array(matrix,arr):
	order = np.argsort(np.array(arr))
	sorted_c = matrix[order]
	for i in range(0,len(sorted_c)):
		sorted_c[i] = sorted_c[i][order]
	return sorted_c




def plot(to_plot):
	#n_rand_all = np.sort(n_rand_all,order =xvalues)
	#get width of matrix
	width  = len(to_plot)
	#get height of matrix 
	height = len(to_plot[0])
	for i in range(0,height):
		print to_plot[i].sum(),",",to_plot[i][i],"indAcc",1.0*to_plot[i][i]/to_plot[i].sum(),"prop",1.0*to_plot[i].sum()/to_plot.sum()
	fig = plt.figure()
	plt.clf()
	ax = fig.add_subplot(111)
	ax.set_aspect(1)
	res = ax.imshow(np.array(to_plot),cmap="RdYlGn",interpolation = 'nearest',vmin=0, vmax=100)#"PuBuGn",

	#annotate
	for x in xrange(width):
		for y in xrange(height):
			ax.annotate(str(to_plot[x][y]),xy=(y,x),
							horizontalalignment = 'center',
							verticalalignment = 'center',
							size = 16)
				


	colourbar = fig.colorbar(res)

	font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 16}

	plt.rc('font', **font)

	xvalues = [1,22,43,64]
	yvalues = [49,33,17,1]
	plt.xticks(range(width),xvalues[:width])
	plt.yticks(range(height),yvalues[:height])

	plt.xlabel('Fields per rule')
	plt.ylabel('Rules per class')

	plt.show()

	#plt.matshow(naive_rand_all)
	#plt.set_cmap("PuBuGn")
	#plt.colorbar()
	#plt.show()





case1_training= np.array([	
	[87,100,99,99],
	[90,100,98,98],
	[84,93,87,87],
	[37,34,11,10]
	])

case1_test= np.array([		
	[50.6836,23.5547,2.55859,0.332031],
	[43.1055,20.7422,1.62109,0.371094],
	[45.0195,14.9219,0.566406,0.0585938],
	[13.5937,8.4375,0.117188,0.0195312]
	])


case2_training= np.array([	
	[86.4,73.1,45.7,40.7],
	[87.5,66.8,35.2,29.8],
	[85.4,62.8,19.6,16.8],
	[21.4,46.5,18,1.1]
	])

case2_test= np.array([		
	[75.2607,34.1706,2.93839,0.35545],
	[75,35.1896,5,0.189573],
	[72.5355,38.6967,1.82464,0.0947867],
	[15.6872,37.2275,15.5213,0.0236967]
	])


case3_training= np.array([	
	[89.4,64.05,29.1,23.2],
	[88.65,58.6,20.6,16],
	[86.5,58.4,15.25,8.8],
	[22.2,54.45,5.65,0.65]
	])

case3_test= np.array([		
	[81.4286,43.1056,6.02484,0.68323],
	[80.9317,43.882,3.91304,0.15528],
	[77.2671,47.2671,4.62733,0.217391],
	[18.9441,49.8447,4.7205,0]
	])

#plot(case3_training)
plot(case3_test)




